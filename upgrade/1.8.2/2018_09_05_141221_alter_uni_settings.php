<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1536127941
 * @version 1.8.2
 */

class AlterUniSettings {

	/**
	 *  执行更新
	 */
	public function up() {
		if(pdo_fieldexists('uni_settings', 'tplnotice')) {
			pdo_query("ALTER TABLE " . tablename('uni_settings') . " MODIFY COLUMN `tplnotice` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '微信通知模板id';");
		}
		if (!pdo_fieldexists('uni_settings', 'sync_member')) {
			pdo_query("ALTER TABLE " . tablename('uni_settings') . " ADD `sync_member` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '同步粉丝到会员：1是 0否';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		