<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546669165
 * @version 1.8.8
 */

class DropTableUniModulesDefault {

	/**
	 *  执行更新
	 */
	public function up() {
		if(pdo_tableexists('uni_modules_default')) {
			$sql = "DROP TABLE IF EXISTS " . tablename('uni_modules_default');
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		